/* An attempt by Manya Gupta - second year engineering student at Manipal Institute of Technology  to detect faces and perform operations using Open CV 2.4.9 and Visual Studio 2012 software written in c++.
This code solves problem 1 part 2,3 and 4 of the Image Processing
and Computer Vision Toolbox Evaluation of FOSSEE group */

//loading directories of C++ and Open CV modules to perform operations 
#include "stdafx.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;
//In this code I will prepare training faces and use the Fisher Face Recognition model to recognize the particular facial feature among others
// This is the datatype that the PrepareFiles function will return
// PrepareFiles loads csv file, training images, and haar cascade file
struct modelandcascade {
	Ptr<FaceRecognizer> themodel;
	CascadeClassifier thecc;
	int w, h;
};
//this function is taking a string list and haar file which is the haar cascade xml to detect features
modelandcascade PrepareFiles(string& imageslist, string& haarfile){
	modelandcascade datatoreturn;
	ifstream readcsv;
	readcsv.open(imageslist);

	vector<Mat> images; // holds all the training images
	vector<int> labels; //holds the labels (1 for person#1, etc) corresponding to each image in images

	string line, path, classlabel;

	while (getline(readcsv, line)) { // reads each line of "readcsv" into "line"
		stringstream liness(line);
		getline(liness, path, ';');  // first half of each line: file path
		getline(liness, classlabel); // second half of each line: label
		if (!path.empty() && !classlabel.empty()) {
			images.push_back(imread(path, 0));
			labels.push_back(atoi(classlabel.c_str()));
		}
	}
	datatoreturn.w = images[0].cols;
	datatoreturn.h = images[0].rows;
	int comp=2;double threshold=1;
	datatoreturn.themodel = createFisherFaceRecognizer(comp,threshold);//creating Fisher Face Recognition model
	
	
	datatoreturn.themodel->train(images, labels);//training the cascade
	datatoreturn.thecc.load(haarfile);//loading HAAR file
	return datatoreturn;
}

int Parttwo(modelandcascade& returned){
	/*This is the function that does the actual face-recognition. It takes as input
	the model, the cascade classifier, and the height and width of the training-images.*/
	Mat frame; // will hold the current frame
	Mat original; // will hold a copy of the current frame
	Mat gray;  // will hold a grayscale copy of the current frame
	vector< Rect_<int> > faces; // will hold the faces in each frame
	Mat resized; // all images must be same size; this will hold resized img
	int currentp; //holds the current prediction
	string box_text; // holds the label for the box drawn around the current face

	//the following line takes the oscar selfie jpg file which I have saved as ellen.jpg
	frame=imread("C:/Users/HP/Desktop/New folder/ellen.jpg");
	
		int a=-1;
		double b=5.0;
		original = frame.clone();
		cvtColor(original, gray, CV_BGR2GRAY);//converting to grey scale
		returned.thecc.detectMultiScale(gray, faces);//using the trained cascade to detect features

		//Now loop over all of the detected faces to identify each one.
		for (int i = 0; i < faces.size(); i++) {
			cv::resize(gray(faces[i]), resized, Size(returned.w, returned.h), 1.0, 1.0, INTER_CUBIC);
			int label=-1;double conf=0.0;
			returned.themodel->predict(resized,label,conf);//prediction takes place here
			
			int w=faces[i].height;
			//cout<<w<<'$';
			/*I have used to above line to find out the height of the first two features detected because it will be of ellen degeneres
			as her face will be detected first and all the others will be rejected the above line gives us the output of only ellen's face 
			hence we are checking for width 26 and 29 in the following line as they are the first two eyes detected and we are rejecting other eyes which might
			have got recognized by our trained cascade */
			if(w==26 || w==29)
			{
			rectangle(original, faces[i], CV_RGB(0, 255, 0), 1);//drawing rectangle around the eyes
			Point center=Point(faces[i].x + (faces[i].width/2) ,faces[i].y + (faces[i].height/2));//finding centroid of rectangle of eye
			
			//calculating the BGR values
			int b=original.at<cv::Vec3b>(center.x,center.y)[0];//blue 
			int g=original.at<cv::Vec3b>(center.x,center.y)[1];//green
			int r=original.at<cv::Vec3b>(center.x,center.y)[2];//red
			//printing the BGR  values on the console
			cout<<" blue value is " <<b;
			cout<< "green value is " << g;
			cout<<"red value is "<< r;
			 
			}
			
		}
		imshow("face_recognizer", original);

		// Display the frame with face-boxes:
		char key = (char)waitKey(200000000);
	
	return 0;
}

int main(int argc, const char *argv[]) {
	//Get filenames for the training images and Haar cascades
	string imageslist, haarfile;
	cout << "Enter name of the file with list of training images, e.g. f.csv: ";
	cin >> imageslist;
	cout << "Enter name of the file with the Haar Cascades, e.g. a.xml: ";
	cin >> haarfile;
	/*the following two lines sets the default file locations which will depend on the location of installation of your open cv and 
	the CSV file created.The CSV file will have files like ..location/1.pgm;0 where 0 will be the label and 1.pgm will be the image of 
	the faces to detect ,in our case of Ellen Degeneres .Also i have included the .csv file and pgm files in the extra files for reference folder 
	which is inside ConsoleApplication1 folder respectively.*/


	haarfile="D:/Users/HP/Desktop/opencv/sources/data/haarcascades/haarcascade_eye.xml";
	imageslist="C:/Users/HP/Desktop/New folder/yoyo.csv";
	// Part 1: Prepare the training images and Haar cascades
	modelandcascade mac = PrepareFiles(imageslist, haarfile);

	// Part 2: using training images and cascades, do face recognition
	Parttwo(mac); 
	return 0;
} 
/* sources used: http://docs.opencv.org/2.4/modules/contrib/doc/facerec/facerec_tutorial.html */