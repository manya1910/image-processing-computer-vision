/* An attempt by Manya Gupta - second year engineering student at Manipal Institute of Technology  to detect fishes and perform operations using Open CV and Visual Studio software written in c++.
This code solves problem 2 part 1 and 2 of the Image Processing
and Computer Vision Toolbox Evaluation of FOSSEE group */
/* In this code I have used inrange function for Object Detection which is in this case  the jelly fishes 
and the contour method for shape recognition */
//loading directories of C++ and Open CV modules to perform operations 


//#include "stdafx.h" uncomment if using Visual Studio 2012
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

/** @function main */
/* To detect the jellyfishes i will use the inrange function which produces an image according to the pixel values 
so we will give the input and output pixels within the range of violet colour so that we get all the jellyfishes*/
int main(int argc, char** argv)
{
	Mat src, src_hsv, res;

	// Read the image which is the jellyfishes image which i have saved as image .jpg
	src = imread("image .jpg");


	// Convert it to gray
	cvtColor(src, src_hsv, CV_BGR2HSV);
	//set the range of pixels for the purple colour as the jellyfishes are purple in colour
	cv::Scalar   min(75, 0, 130);//rgb values for indigo 
	cv::Scalar   max(230, 230, 250);//rgb values for lavender

	inRange(src_hsv, min, max, res);//performing function
	
	imshow(" 3", res);//showing the detected jellyfishes
	/*for the next part to detect the centroid in each jellyfish we will use the findContours method which finds contours in an image and helps
	us in shape recognition.After that we find the minimum enclosing cirle around that contour and hence we then identify
	cirles around the fishes and find out the centroid of each jellyfish.We will then mark a red cross on each centroid*/
	vector<vector<Point> > contours;
	vector<cv::Vec4i> heirarchy;
	vector<cv::Point2i> center;
	vector<int> radius;
	Mat threshold_output;
	cv::findContours(res, contours, heirarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE);//finding contours
	size_t count = contours.size();

	for (int i = 0; i<count; i++)//looping for all the detected contours
	{
		cv::Point2f c;
		float r;
		cv::minEnclosingCircle(contours[i], c, r);//finding the minimum enclosing circle around the point set 

		if (r >= 16 && r < 52)// Reduce the noise so we avoid false circle detection
		{

			center.push_back(c);
			radius.push_back(r);
		}
	}
	size_t countt = center.size();//storing centers of fishes 
	cv::Scalar red(0, 0, 255);//for the red cross 

	for (int i = 0; i < countt; i++)
	{
		//cv::circle(src, center[i], radius[i], red, 1.5);
		//the following code will make lines horizontally and vertically to make the red cross
		Point b = center[i];
		b.x = b.x + 5;
		Point d = center[i];
		d.x = d.x - 5;
		Point c = center[i];
		c.y = c.y + 5;
		Point e = center[i];
		e.y = e.y - 5;
		//now drawing lines on the image to make crosses
		cv::line(src,
			center[i],
			e,
			red,
			1);
		cv::line(src,
			center[i],
			d,
			red,
			1);
		cv::line(src,
			center[i],
			b,
			red,
			1);
		cv::line(src,
			center[i],
			c,
			red,
			1);
	}
	imshow("result", src);//showing the result image

	waitKey(0);
	return 0;
}
/*sources : 1. http://docs.opencv.org/master/da/d97/tutorial_threshold_inRange.html 
2. http://docs.opencv.org/2.4/modules/imgproc/doc/structural_analysis_and_shape_descriptors.html
3 http://www.rapidtables.com/web/color/purple-color.htm (for the rgb values of range of purple colors from indigo to lavender */