/* An attempt by Manya Gupta - second year engineering student at Manipal Institute of Technology  to detect faces and perform operations using Open CV 2.4.9 and Visual Studio 2012 software written in c++.
This code solves problem 1 part 1 of the Image Processing
and Computer Vision Toolbox Evaluation of FOSSEE group */
/* In this code I have used Haar Feature-based Cascade Classifier for Object Detection which is in this case faces of the 
people taking the Oscar Selfie */
//loading directories of C++ and Open CV modules to perform operations 


#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;



int main()
{
Mat image;
	image = imread("image .jpg", CV_LOAD_IMAGE_COLOR);//reading the oscar selfie image as image .jpg file
	namedWindow("window1", 1);   imshow("window1", image);//showing the original image

	// Load Face cascade (.xml file) which will depend on the installation path of Open CV
	CascadeClassifier face_cascade;
	face_cascade.load("C:/OpenCV-3.2.0/opencv/build/etc/haarcascades/haarcascade_frontalface_alt.xml");
	CascadeClassifier eye_cascade;
	eye_cascade.load("C:/OpenCV-3.2.0/opencv/build/etc/haarcascades/haarcascade_eye.xml");
	// Detect faces
	std::vector<Rect> faces;
	face_cascade.detectMultiScale(image, faces, 1.1, 1, 0 | CV_HAAR_SCALE_IMAGE, Size(100,100));

	// Draw circles on the detected faces
	for (int i = 0; i < faces.size(); i++)
	{
		
	
		Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5);//calculating center
		ellipse(image, center, Size(faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar(255, 0, 255), 1, 4, 0);//drawing an ellipse over the detected faces 
		
	}
	
	imshow("Detected Face", image);//showing the detected faces in a new window 

	waitKey(0);
	return 0; 
}
// sources used : http://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html 